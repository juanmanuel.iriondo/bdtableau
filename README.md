# Practica de Big Data con Tableau

## Fuentes de datos usadas

Airbnb 
Actividades culturales de Madrid (obtenido de una fuente de datos públicos del ayuntamiento de Madrid en formato CSV)

## Resumen

Mi práctica se trata de ver las actividades culturales del ayuntamiento de Madrid de los próximos meses y viendo en que distrito de Madrid
se desarrollan tomar una decisión sobre en que distrito alojarse en pisos Airbnb viendo el precio medio de coste de la zona.
También tomar esta decisión viendo el número mínimo de noches medio quehay que alojarse en los pisos de airbnb en los distintos distritos de Madrid. 

| Fuente de datos | Campo                 | Significado            |
| --------------- | --------------------- | ---------------------- |
| Airbnb          | Price                 | Precio                 |
| Airbnb          | NeighbourGroupCleaned | Distrito de Madrid     |
| Airbnb          | Minimum Nights        | Número mínimo de noches|
| Actividades     | Distrito              | Distrito de Madrid     |


***Enlace al fichero ActCulMad de Tableau***

https://drive.google.com/file/d/1vUTFacZa66zCyDzY1Qs6VWwyNchQwAbO/view?usp=sharing

## Notas

He importado los datos en local porque aunque he conseguido importarlos desde Google Cloud de MySQL, no he conseguido que la latitud y la longitud
se importe bien. He probado con diferentes tipos de datos (Decimal, Float, Varchar casteando luego en Tableau a Float) pero lo hacía mal porque seguramente el dato
en el fichero csv al cambiar el separador ";" por separador "," hice algo mal.



